//Função AJAX para Logar no sistema.
$(document).ready(function(){
        $('#loginGif').hide();
        $('#erroLogin').hide();
            $('#formLogin').submit(function(){  //Ao submeter formulário
                $('#loginGif').show();
                $('#erroLogin').hide();
                var login=$('#username').val();
                var senha=$('#psw').val();
                $.ajax({            
                    url:"validacao.php",                  
                    type:"post",                            
                    data: "login="+login+"&senha="+senha,   //Dados
                    success: function (result){             //Sucesso no AJAX
                        if (result ==1){
                            $('#loginGif').hide();
                            window.location.href = "red.php";
                        }if (result !=1){
                            $('#loginGif').hide();
                            $("#erroLogin").html("<p class='text-center'>Desculpe: "+ result+ "</p>");
                            $('#erroLogin').show();
                            $('#erroLogin').addClass('animated shake');
                        }  
                    }
                });
            return false;   //Evita que a página seja atualizada
            });
});

$(document).ready(function(){
  // Código para adicionar suavidade no deslizamento da página através do menu.
  $(".navbar a, footer a[href='#myPage'], .btnEntraEmContato").on('click', function(event) {
    //Certifique-se de que this.hash tem um valor antes de substituir o comportamento padrão
    if (this.hash !== "") {
      // Prevenir o comportamento de clique de âncora padrão
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Usando o método jQuery animate () para adicionar scroll de página suave
      // Um número opcional (900) que significa o número de milissegundos necessários para rolar para a área escolhida.
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Adicionar hash(#) ao URL quando feito scroll (comportamento de clique padrão)
        window.location.hash = hash;
      });
    } // Fim do IF
  });
  

  //Código para quando deslizar a página ele aparecer os elementos.
  $(window).scroll(function() {
    $(".esconderParaDeslizar").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
});

$(document).ready(function(){
    $("#myLogin").click(function(){
        $("#modalLogin").modal();
    });
});


//Função AJAX para contato
$(document).ready(function(){
        $('#loadGif').hide();
        $('#sucessoEnvio').hide();
        $('#erroEnvio').hide();
        $('.imputForm').hide();
            $('#formularioContato').submit(function(){  //Ao submeter formulário
                $('#loadGif').show();
                $('#sucessoEnvio').hide();
                $('#erroEnvio').hide();
                var nome=$('#nome').val();
                var email=$('#email').val();
                var comentario=$('#comentario').val();
                $.ajax({            //Função AJAX
                    url:"db/solicitacaoContato.php",                    //Arquivo php
                    type:"post",                            //Método de envio
                    data: "nome="+nome+"&email="+email+"&comentario="+comentario,   //Dados
                    success: function (result){             //Sucesso no AJAX
                        if(result==0){
                            $('#loadGif').hide();
                            $("#erroEnvio").html("<p class='text-center'>Erro de inserção na base de dados.</p>");
                            $('#erroEnvio').show();
                            $('#erroEnvio').addClass('animated shake');
                        }if(result==1){
                            $('#loadGif').hide();
                            $('#fieldsetContato').attr("disabled", "disabled");
                            $('.imputForm').show();
                            $(".form-group").addClass("has-success has-feedback");
                            //$(".imputForm").append("<span class='glyphicon glyphicon-ok form-control-feedback'></span>");
                            
                            $('#sucessoEnvio').show();
                            $('#sucessoEnvio').addClass('animated shake');                   
                        }if (result ==2){
                            $('#loadGif').hide();
                            $("#erroEnvio").html("<p class='text-center'>Ops, Um dos contatos não foi enviado.</p>");
                            $('#erroEnvio').show();
                            $('#erroEnvio').addClass('animated shake');
                        }if (result !=0 && result != 1 && result != 2){
                            //$('#loadGif').hide();
                            //$("#erroEnvio").html("<p class='text-center'>Ops,"+result+" ocorreu um pequeno erro no envio do e-mail, mas não tem problema.</br>Você ainda pode ligar para nosso número: (81) 0000-0000.</p>");
                            //$('#erroEnvio').show();
                            //$('#erroEnvio').addClass('animated shake');
                            $('#loadGif').hide();
                            $('#fieldsetContato').attr("disabled", "disabled");
                            $('.imputForm').show();
                            $(".form-group").addClass("has-success has-feedback");
                            //$(".imputForm").append("<span class='glyphicon glyphicon-ok form-control-feedback'></span>");
                            
                            $('#sucessoEnvio').show();
                            $('#sucessoEnvio').addClass('animated shake');
                        }  
                    }
                });
            return false;   //Evita que a página seja atualizada
            });
});

//Função AJAX para Cadastro de Usuário
$(document).ready(function(){
        $('#loadCadastro').hide();
        $('#sucessoCadastro').hide();
        $('#erroCadastro').hide();
            $('#formCadastro').submit(function(){  //Ao submeter formulário
                $('#loadCadastro').show();
                $('#sucessoCadastro').hide();
                $('#erroCadastro').hide();
                var nome=$('#nome').val();
                var email=$('#email').val();
                var telefone=$('#telefone').val();
                var login=$('#login').val();
                var senha=$('#senha').val();
                var datenasc=$('#datenasc').val();
                $.ajax({            //Função AJAX
                    url:"db/cadastroUser.php",                    //Arquivo php
                    type:"post",                            //Método de envio
                    data: "nome="+nome+"&email="+email+"&telefone="+telefone+"&login="+login+"&senha="+senha+"&datenasc="+datenasc,   //Dados
                    success: function (result){             //Sucesso no AJAX
                        if(result==1){
                            $('#loadCadastro').hide();
                            $('#fieldsetCadastro').attr("disabled", "disabled");
                            $('.imputForm').show();
                            $(".form-group").addClass("has-success has-feedback");
                            $('#sucessoCadastro').show();
                            $('#sucessoCadastro').addClass('animated shake');                   
                        }if (result == 2){
                            $('#loadCadastro').hide();
                            $('#erroCadastro').show();
                            $("#erroCadastro").html("<p class='text-center'>Você foi cadastrado, mas não conseguimos te enviar um e-mail de confirmação: </p>");
                            $('#erroCadastro').addClass('animated shake');
                        }if (result !=1 && result != 2 ){
                            $('#loadCadastro').hide();
                            $("#erroCadastro").html("<p class='text-center'>Desculpe: "+ result+ "</p>");
                            $('#erroCadastro').show();
                            $('#erroCadastro').addClass('animated shake');
                        }  
                    }
                });
            return false;   //Evita que a página seja atualizada
            });
});

function formatar(mascara, documento){
  var i = documento.value.length;
  var saida = mascara.substring(0,1);
  var texto = mascara.substring(i)
  
  if (texto.substring(0,1) != saida){
            documento.value += texto.substring(0,1);
  }
  
}